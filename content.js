let scripts = document.getElementsByTagName('script')
for (let i = 0; i < scripts.length; i++) {
  if (scripts[i].innerHTML.match(/var player/)) {
    let matches = scripts[i].innerHTML.match(/(http.+?)[,"]/)
    if (matches) {
      fetch(matches[1])
      .then(response => { return response.text() } )
      .then(data => {
        let urls = data.split('\n')
        urls.pop()
        urls.pop()
        let lastURL = urls.pop()
        matches = lastURL.match(/\/([^\/]+?)\/seg-(\d+)/)
        if (matches) {
          lastURL = lastURL.replace(/seg-\d+/, `seg-[1-${matches[2]}]`)
          let divTitle = document.getElementsByClassName('details__title')[0],
              myDiv = document.createElement('div')
          myDiv.setAttribute('style', 'color: #FFFFFF; font-size: 12px;')
          myDiv.innerHTML = `curl '${lastURL}' >> ${matches[1]}; ffmpeg -i ${matches[1]} -c copy -bsf:a aac_adtstoasc timing_fixed_${matches[1]}`
          divTitle.insertBefore(myDiv, divTitle.childNodes[0])
        }
      })
      .catch(err => {})
    }
  }
}